/****** Script for SelectTopNRows command from SSMS  ******/
-- #labelQuery_old is to save data table before 2016/8/22 with old DataName
IF(OBJECT_ID('tempdb..#labelQuery_old') IS NOT NULL)
BEGIN
    DROP TABLE #labelQuery_old
END;
-- #labelQuery is to save data table after 2016/8/22 with new DataName
IF(OBJECT_ID('tempdb..#labelQuery') IS NOT NULL)
BEGIN
    DROP TABLE #labelQuery
END;

-- query data after 2016/8/22
WITH labelQuery AS (SELECT label.ContainerName, label.DataName, label.DataValue, label.TxnDate
  FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData] label
  JOIN [CamODS_6X].[insitedb_schema].[Container] c
  ON label.ContainerName = c.ContainerName
  JOIN [CamODS_6X].[insitedb_schema].[ContainerDetail] cd
  ON c.ContainerId = cd.ContainerId
  WHERE label.DataCollectionDefName = 'LabelVerification_NFC' and label.TxnDate > '2016-08-25'
)
SELECT labelQuery.ContainerName, labelQuery.DataName, labelQuery.DataValue, labelQuery.TxnDate
INTO #labelQuery
FROM labelQuery
JOIN (SELECT ContainerName, DataName, max(TxnDate) TxnDate_max
FROM labelQuery GROUP BY ContainerName, DataName) label_txn_max
ON labelQuery.ContainerName = label_txn_max.ContainerName and labelQuery.DataName = label_txn_max.DataName and labelQuery.TxnDate=label_txn_max.TxnDate_max
GO

-- query data before 2016/8/22 where DataName had prefix "NCF". 
WITH labelQuery AS (SELECT label.ContainerName, label.DataName, label.DataValue, label.TxnDate
  FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData] label
  JOIN [CamODS_6X].[insitedb_schema].[Container] c
  ON label.ContainerName = c.ContainerName
  JOIN [CamODS_6X].[insitedb_schema].[ContainerDetail] cd
  ON c.ContainerId = cd.ContainerId
  WHERE label.DataCollectionDefName = 'LabelVerification_NFC' and label.TxnDate < '2016-08-25'
)
SELECT labelQuery.ContainerName, labelQuery.DataName, labelQuery.DataValue, labelQuery.TxnDate
INTO #labelQuery_old
FROM labelQuery
JOIN (SELECT ContainerName, DataName, max(TxnDate) TxnDate_max
FROM labelQuery GROUP BY ContainerName, DataName) label_txn_max
ON labelQuery.ContainerName = label_txn_max.ContainerName and labelQuery.DataName = label_txn_max.DataName and labelQuery.TxnDate=label_txn_max.TxnDate_max
GO

-- merge old and new data together. Note that old and new DataName are very different, thus need a lot of handling. And "Union" actually failed due to "Conversion failed when converting the nvarchar value '..float..' to data type int."
with data_old as 
	(SELECT TxnDate as VerifyDate,[NFC A1],[NFC G3],[NFC Tray ID],[NFC Tray Version],[NFC Lot ID],[NFC Pkmid 1],[NFC T1],[NFC T3],[NFC C2],[NFC G1],[NFC Substrate 4],[NFC A4],[NFC Laser Power 4],[NFC Pkmid 2],[NFC T2],[NFC A2],[NFC C1],[NFC C3],[NFC Substrate 3],[NFC Laser Power 2],[NFC Pkmid 4],[NFC Schema Version],[NFC Sensor Gain],[NFC Substrate 2],[NFC G2],[NFC Laser Power 1],[NFC Laser Power 3],[NFC A3],[NFC C4],[NFC Part Number],[NFC Substrate 1],[NFC T4]
	FROM 
	(SELECT DataName, DataValue, TxnDate FROM #labelQuery_old) p 
	PIVOT
	(max(DataValue)
	FOR DataName IN ([NFC A1],[NFC G3],[NFC Tray ID],[NFC Tray Version],[NFC Lot ID],[NFC Pkmid 1],[NFC T1],[NFC T3],[NFC C2],[NFC G1],[NFC Substrate 4],[NFC A4],[NFC Laser Power 4],[NFC Pkmid 2],[NFC T2],[NFC A2],[NFC C1],[NFC C3],[NFC Substrate 3],[NFC Laser Power 2],[NFC Pkmid 4],[NFC Schema Version],[NFC Sensor Gain],[NFC Substrate 2],[NFC G2],[NFC Laser Power 1],[NFC Laser Power 3],[NFC A3],[NFC C4],[NFC Part Number],[NFC Substrate 1],[NFC T4])
	) AS pvt)
select 
VerifyDate, [NFC Tray ID] as [Tray ID], [NFC Schema Version] as [Schema Version], [NFC Part Number] as [Part Number], [NFC Tray Version] as [Tray Version],
0 as [CellRevision], [NFC Lot ID] as [Lot ID], '0' as [ExpirationDate], '' as [GreenPSF], '' as [RedPSF], 0 as [PickupForce], [NFC Substrate 1] as [SerialNumber1], 

'' as [State1],0 as [AmplifierGainSetting1],0 as [SensorGain1], [NFC T1] as [SpectralCalT1], [NFC G1] as [SpectralCalG1], [NFC C1] as [SpectralCalC1],[NFC A1] as [SpectralCalA1],0 as [DrySortCt1], 0 as [LightBrushCt1], --[NFC Sensor Gain]

0 as [LightBrushAt1],0 as [LightBrushPitch1],0 as [LightBrushYaw1],0 as [LightBrushRotation1],0 as [LoadingRatio1],[NFC Laser Power 1] as [InitialLaserPowerRatio1.1], [NFC Pkmid 1] as [Pkmid1.1], 0 as [Snr1.1],

0 as [InitialLaserPowerRatio1.2], 0 as [Pkmid1.2], 0 as [Snr1.2], 0 as [InitialLaserPowerRatio1.3],0 as [Pkmid1.3], 0 as [Snr1.3],0 as [InitialLaserPowerRatio1.4],0 as [Pkmid1.4],0 as [Snr1.4],

[NFC Substrate 2] as [SerialNumber2],'' as [State2],0 as [AmplifierGainSetting2],0 as [SensorGain2],[NFC T2] as [SpectralCalT2],[NFC G2] as [SpectralCalG2],[NFC C2] as [SpectralCalC2],[NFC A2] as [SpectralCalA2], 0 as [DrySortCt2],

0 as [LightBrushCt2],0 as [LightBrushAt2],0 as [LightBrushPitch2],0 as [LightBrushYaw2],0 as [LightBrushRotation2],0 as [LoadingRatio2],[NFC Laser Power 2] as[InitialLaserPowerRatio2.1],[NFC Pkmid 2] as [Pkmid2.1],0 as[Snr2.1],

0 as [InitialLaserPowerRatio2.2],0 as [Pkmid2.2],0 as [Snr2.2],0 as [InitialLaserPowerRatio2.3],0 as [Pkmid2.3],0 as [Snr2.3],0 as [InitialLaserPowerRatio2.4], 0 as [Pkmid2.4],0 as [Snr2.4],

[NFC Substrate 3] as [SerialNumber3],'' as [State3],0 as [AmplifierGainSetting3],0 as [SensorGain3],[NFC T3] as [SpectralCalT3],[NFC G3] as [SpectralCalG3],[NFC C3] as [SpectralCalC3],[NFC A3] as [SpectralCalA3],0 as [DrySortCt3],

0 as [LightBrushCt3],0 as [LightBrushAt3],0 as [LightBrushPitch3],0 as [LightBrushYaw3],0 as [LightBrushRotation3],0 as [LoadingRatio3],[NFC Laser Power 3] as [InitialLaserPowerRatio3.1],0 as [Pkmid3.1],0 as[Snr3.1],

0 as [InitialLaserPowerRatio3.2],0 as [Pkmid3.2],0 as [Snr3.2],0 as [InitialLaserPowerRatio3.3],0 as [Pkmid3.3],0 as [Snr3.3],0 as [InitialLaserPowerRatio3.4],0 as [Pkmid3.4],0 as [Snr3.4],

[NFC Substrate 4] as [SerialNumber4],'' as [State4],0 as [AmplifierGainSetting4],0 as [SensorGain4],[NFC T4] as [SpectralCalT4],0 as [SpectralCalG4],[NFC C4] as [SpectralCalC4],[NFC A4] as [SpectralCalA4],0 as [DrySortCt4],

0 as [LightBrushCt4],0 as [LightBrushAt4],0 as [LightBrushPitch4],0 as [LightBrushYaw4],0 as [LightBrushRotation4],0 as [LoadingRatio4],[NFC Laser Power 4] as [InitialLaserPowerRatio4.1],[NFC Pkmid 4] as [Pkmid4.1],0 as [Snr4.1],

0 as  [InitialLaserPowerRatio4.2],0 as [Pkmid4.2],0 as [Snr4.2],0 as [InitialLaserPowerRatio4.3],0 as [Pkmid4.3],0 as [Snr4.3],0 as [InitialLaserPowerRatio4.4],0 as [Pkmid4.4],0 as [Snr4.4]

from data_old

UNION ALL

(SELECT 
TxnDate as VerifyDate,[TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],
[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],
[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1],
[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],
[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],
[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],
[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],
[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],
[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],
[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],
[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],
[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],
[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4]
--TxnDate as VerifyDate,[TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1]--,[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4]
FROM 
(SELECT DataName, DataValue, TxnDate FROM #labelQuery) p
PIVOT
(max(DataValue)
FOR DataName IN ([TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1],[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4])
) AS pvt)





