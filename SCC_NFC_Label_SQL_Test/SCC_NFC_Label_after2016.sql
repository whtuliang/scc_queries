-- #labelQuery is to save data table after 2016/8/22 with new DataName
IF(OBJECT_ID('tempdb..#labelQuery') IS NOT NULL)
BEGIN
    DROP TABLE #labelQuery
END;

-- query data after 2016/8/22
WITH labelQuery AS (SELECT label.ContainerName, label.DataName, label.DataValue, label.TxnDate
  FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData] label
  JOIN [CamODS_6X].[insitedb_schema].[Container] c
  ON label.ContainerName = c.ContainerName
  JOIN [CamODS_6X].[insitedb_schema].[ContainerDetail] cd
  ON c.ContainerId = cd.ContainerId
  WHERE label.DataCollectionDefName = 'LabelVerification_NFC' and label.TxnDate > '2016-08-25'
)
SELECT labelQuery.ContainerName, labelQuery.DataName, labelQuery.DataValue, labelQuery.TxnDate
INTO #labelQuery
FROM labelQuery
JOIN (SELECT ContainerName, DataName, max(TxnDate) TxnDate_max
FROM labelQuery GROUP BY ContainerName, DataName) label_txn_max
ON labelQuery.ContainerName = label_txn_max.ContainerName and labelQuery.DataName = label_txn_max.DataName and labelQuery.TxnDate=label_txn_max.TxnDate_max
GO

--TxnDate as VerifyDate,[TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1]--,[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4]
(SELECT 
TxnDate as VerifyDate,[TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],
[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],
[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1],
[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],
[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],
[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],
[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],
[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],
[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],
[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],
[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],
[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],
[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4]
FROM 
(SELECT DataName, DataValue, TxnDate FROM #labelQuery) p
PIVOT
(max(DataValue)
FOR DataName IN ([TrayID],[SchemaVersion],[PartNumber],[TrayVersion],[CellRevision],[LotID],[ExpirationDate],[GreenPSF],[RedPSF],[PickupForce],[SerialNumber1],[State1],[AmplifierGainSetting1],[SensorGain1],[SpectralCalT1],[SpectralCalG1],[SpectralCalC1],[SpectralCalA1],[DrySortCt1],[LightBrushCt1],[LightBrushAt1],[LightBrushPitch1],[LightBrushYaw1],[LightBrushRotation1],[LoadingRatio1],[InitialLaserPowerRatio1.1],[Pkmid1.1],[Snr1.1],[InitialLaserPowerRatio1.2],[Pkmid1.2],[Snr1.2],[InitialLaserPowerRatio1.3],[Pkmid1.3],[Snr1.3],[InitialLaserPowerRatio1.4],[Pkmid1.4],[Snr1.4],[SerialNumber2],[State2],[AmplifierGainSetting2],[SensorGain2],[SpectralCalT2],[SpectralCalG2],[SpectralCalC2],[SpectralCalA2],[DrySortCt2],[LightBrushCt2],[LightBrushAt2],[LightBrushPitch2],[LightBrushYaw2],[LightBrushRotation2],[LoadingRatio2],[InitialLaserPowerRatio2.1],[Pkmid2.1],[Snr2.1],[InitialLaserPowerRatio2.2],[Pkmid2.2],[Snr2.2],[InitialLaserPowerRatio2.3],[Pkmid2.3],[Snr2.3],[InitialLaserPowerRatio2.4],[Pkmid2.4],[Snr2.4],[SerialNumber3],[State3],[AmplifierGainSetting3],[SensorGain3],[SpectralCalT3],[SpectralCalG3],[SpectralCalC3],[SpectralCalA3],[DrySortCt3],[LightBrushCt3],[LightBrushAt3],[LightBrushPitch3],[LightBrushYaw3],[LightBrushRotation3],[LoadingRatio3],[InitialLaserPowerRatio3.1],[Pkmid3.1],[Snr3.1],[InitialLaserPowerRatio3.2],[Pkmid3.2],[Snr3.2],[InitialLaserPowerRatio3.3],[Pkmid3.3],[Snr3.3],[InitialLaserPowerRatio3.4],[Pkmid3.4],[Snr3.4],[SerialNumber4],[State4],[AmplifierGainSetting4],[SensorGain4],[SpectralCalT4],[SpectralCalG4],[SpectralCalC4],[SpectralCalA4],[DrySortCt4],[LightBrushCt4],[LightBrushAt4],[LightBrushPitch4],[LightBrushYaw4],[LightBrushRotation4],[LoadingRatio4],[InitialLaserPowerRatio4.1],[Pkmid4.1],[Snr4.1],[InitialLaserPowerRatio4.2],[Pkmid4.2],[Snr4.2],[InitialLaserPowerRatio4.3],[Pkmid4.3],[Snr4.3],[InitialLaserPowerRatio4.4],[Pkmid4.4],[Snr4.4])
) AS pvt)