/****** Script for SelectTopNRows command from SSMS  ******/
-- #labelQuery_old is to save data table before 2016/8/22 with old DataName
IF(OBJECT_ID('tempdb..#labelQuery_old') IS NOT NULL)
BEGIN
    DROP TABLE #labelQuery_old
END;


-- query data before 2016/8/22 where DataName had prefix "NCF". 
WITH labelQuery AS (SELECT label.ContainerName, label.DataName, label.DataValue, label.TxnDate
  FROM [CamODS_6X].[insitedb_schema].[PBI_ParametricData] label
  JOIN [CamODS_6X].[insitedb_schema].[Container] c
  ON label.ContainerName = c.ContainerName
  JOIN [CamODS_6X].[insitedb_schema].[ContainerDetail] cd
  ON c.ContainerId = cd.ContainerId
  WHERE label.DataCollectionDefName = 'LabelVerification_NFC' and label.TxnDate < '2016-08-25'
)
SELECT labelQuery.ContainerName, labelQuery.DataName, labelQuery.DataValue, labelQuery.TxnDate
INTO #labelQuery_old
FROM labelQuery
JOIN (SELECT ContainerName, DataName, max(TxnDate) TxnDate_max
FROM labelQuery GROUP BY ContainerName, DataName) label_txn_max
ON labelQuery.ContainerName = label_txn_max.ContainerName and labelQuery.DataName = label_txn_max.DataName and labelQuery.TxnDate=label_txn_max.TxnDate_max
GO

-- merge old and new data together. Note that old and new DataName are very different, thus need a lot of handling. And "Union" actually failed due to "Conversion failed when converting the nvarchar value '..float..' to data type int."
with data_old as 
	(SELECT TxnDate as VerifyDate,[NFC A1],[NFC G3],[NFC Tray ID],[NFC Tray Version],[NFC Lot ID],[NFC Pkmid 1],[NFC T1],[NFC T3],[NFC C2],[NFC G1],[NFC Substrate 4],[NFC A4],[NFC Laser Power 4],[NFC Pkmid 2],[NFC T2],[NFC A2],[NFC C1],[NFC C3],[NFC Substrate 3],[NFC Laser Power 2],[NFC Pkmid 4],[NFC Schema Version],[NFC Sensor Gain],[NFC Substrate 2],[NFC G2],[NFC Laser Power 1],[NFC Laser Power 3],[NFC A3],[NFC C4],[NFC Part Number],[NFC Substrate 1],[NFC T4]
	FROM 
	(SELECT DataName, DataValue, TxnDate FROM #labelQuery_old) p 
	PIVOT
	(max(DataValue)
	FOR DataName IN ([NFC A1],[NFC G3],[NFC Tray ID],[NFC Tray Version],[NFC Lot ID],[NFC Pkmid 1],[NFC T1],[NFC T3],[NFC C2],[NFC G1],[NFC Substrate 4],[NFC A4],[NFC Laser Power 4],[NFC Pkmid 2],[NFC T2],[NFC A2],[NFC C1],[NFC C3],[NFC Substrate 3],[NFC Laser Power 2],[NFC Pkmid 4],[NFC Schema Version],[NFC Sensor Gain],[NFC Substrate 2],[NFC G2],[NFC Laser Power 1],[NFC Laser Power 3],[NFC A3],[NFC C4],[NFC Part Number],[NFC Substrate 1],[NFC T4])
	) AS pvt)
select 
VerifyDate, [NFC Tray ID] as [Tray ID], [NFC Schema Version] as [Schema Version], [NFC Part Number] as [Part Number], [NFC Tray Version] as [Tray Version],
0 as [CellRevision], [NFC Lot ID] as [Lot ID], '0' as [ExpirationDate], '' as [GreenPSF], '' as [RedPSF], 0 as [PickupForce], [NFC Substrate 1] as [SerialNumber1], 

'' as [State1],0 as [AmplifierGainSetting1],0 as [SensorGain1], [NFC T1] as [SpectralCalT1], [NFC G1] as [SpectralCalG1], [NFC C1] as [SpectralCalC1],[NFC A1] as [SpectralCalA1],0 as [DrySortCt1], 0 as [LightBrushCt1], --[NFC Sensor Gain]

0 as [LightBrushAt1],0 as [LightBrushPitch1],0 as [LightBrushYaw1],0 as [LightBrushRotation1],0 as [LoadingRatio1],[NFC Laser Power 1] as [InitialLaserPowerRatio1.1], [NFC Pkmid 1] as [Pkmid1.1], 0 as [Snr1.1],

0 as [InitialLaserPowerRatio1.2], 0 as [Pkmid1.2], 0 as [Snr1.2], 0 as [InitialLaserPowerRatio1.3],0 as [Pkmid1.3], 0 as [Snr1.3],0 as [InitialLaserPowerRatio1.4],0 as [Pkmid1.4],0 as [Snr1.4],

[NFC Substrate 2] as [SerialNumber2],'' as [State2],0 as [AmplifierGainSetting2],0 as [SensorGain2],[NFC T2] as [SpectralCalT2],[NFC G2] as [SpectralCalG2],[NFC C2] as [SpectralCalC2],[NFC A2] as [SpectralCalA2], 0 as [DrySortCt2],

0 as [LightBrushCt2],0 as [LightBrushAt2],0 as [LightBrushPitch2],0 as [LightBrushYaw2],0 as [LightBrushRotation2],0 as [LoadingRatio2],[NFC Laser Power 2] as[InitialLaserPowerRatio2.1],[NFC Pkmid 2] as [Pkmid2.1],0 as[Snr2.1],

0 as [InitialLaserPowerRatio2.2],0 as [Pkmid2.2],0 as [Snr2.2],0 as [InitialLaserPowerRatio2.3],0 as [Pkmid2.3],0 as [Snr2.3],0 as [InitialLaserPowerRatio2.4], 0 as [Pkmid2.4],0 as [Snr2.4],

[NFC Substrate 3] as [SerialNumber3],'' as [State3],0 as [AmplifierGainSetting3],0 as [SensorGain3],[NFC T3] as [SpectralCalT3],[NFC G3] as [SpectralCalG3],[NFC C3] as [SpectralCalC3],[NFC A3] as [SpectralCalA3],0 as [DrySortCt3],

0 as [LightBrushCt3],0 as [LightBrushAt3],0 as [LightBrushPitch3],0 as [LightBrushYaw3],0 as [LightBrushRotation3],0 as [LoadingRatio3],[NFC Laser Power 3] as [InitialLaserPowerRatio3.1],0 as [Pkmid3.1],0 as[Snr3.1],

0 as [InitialLaserPowerRatio3.2],0 as [Pkmid3.2],0 as [Snr3.2],0 as [InitialLaserPowerRatio3.3],0 as [Pkmid3.3],0 as [Snr3.3],0 as [InitialLaserPowerRatio3.4],0 as [Pkmid3.4],0 as [Snr3.4],

[NFC Substrate 4] as [SerialNumber4],'' as [State4],0 as [AmplifierGainSetting4],0 as [SensorGain4],[NFC T4] as [SpectralCalT4],0 as [SpectralCalG4],[NFC C4] as [SpectralCalC4],[NFC A4] as [SpectralCalA4],0 as [DrySortCt4],

0 as [LightBrushCt4],0 as [LightBrushAt4],0 as [LightBrushPitch4],0 as [LightBrushYaw4],0 as [LightBrushRotation4],0 as [LoadingRatio4],[NFC Laser Power 4] as [InitialLaserPowerRatio4.1],[NFC Pkmid 4] as [Pkmid4.1],0 as [Snr4.1],

0 as  [InitialLaserPowerRatio4.2],0 as [Pkmid4.2],0 as [Snr4.2],0 as [InitialLaserPowerRatio4.3],0 as [Pkmid4.3],0 as [Snr4.3],0 as [InitialLaserPowerRatio4.4],0 as [Pkmid4.4],0 as [Snr4.4]

from data_old







