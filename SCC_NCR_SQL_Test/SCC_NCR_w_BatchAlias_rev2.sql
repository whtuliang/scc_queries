/****** Script for SelectTopNRows command from SSMS  ******/
--DECLARE @ncrQueryList TABLE (EventName  VARCHAR(100), Initiator VARCHAR(100), 
--Status  VARCHAR(100), Owner VARCHAR(100), ContainerName  VARCHAR(100), ContainerLevel VARCHAR(100), 
--CustomerLot  VARCHAR(10), EventArea VARCHAR(100), Description  VARCHAR(100), OccurrenceDate Datetime, 
--OperationName  VARCHAR(100), Product VARCHAR(100), Qty VARCHAR(100), CloseDate Datetime,
--ClosedBy  VARCHAR(100), Resolution VARCHAR(100), ResolutionComments VARCHAR(100)
--);

IF OBJECT_ID('tempdb..#ncrQueryList') IS NOT NULL
begin
        drop table #ncrQueryList
end

IF OBJECT_ID('tempdb..#ncrQuery') IS NOT NULL
begin
        drop table #ncrQuery
end

IF(OBJECT_ID ('dbo.BatchAlias', 'FN') IS NOT NULL)
BEGIN
    DROP FUNCTION BatchAlias;
END;

IF(OBJECT_ID('tempdb..#BatchAliasQuerySequel') IS NOT NULL)
BEGIN
    DROP TABLE #BatchAliasQuerySequel
END;

IF(OBJECT_ID('tempdb..#BatchAliasQuerySpider') IS NOT NULL)
BEGIN
    DROP TABLE #BatchAliasQuerySpider
END;

GO

CREATE FUNCTION dbo.BatchAlias(@Wafer NVARCHAR(30))
        RETURNS NVARCHAR(30)
      AS
      BEGIN
        RETURN (CASE
            WHEN @Wafer = 'VP51126-07' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-09' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-10' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-12' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-24' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-08' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-14' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-05' THEN 'LVP2a'
            WHEN @Wafer = 'VP51126-11' THEN 'LVP2a'

            WHEN @Wafer = 'VP51126-04' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-15' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-16' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-17' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-18' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-19' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-20' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-21' THEN 'LVP2b'
            WHEN @Wafer = 'VP51126-22' THEN 'LVP2b'

            WHEN @Wafer = 'VP51165-19' THEN 'LVP3a'
            WHEN @Wafer = 'VP51165-20' THEN 'LVP3a'
            WHEN @Wafer = 'VP51165-21' THEN 'LVP3a'
            WHEN @Wafer = 'VP51165-22' THEN 'LVP3a'

            WHEN @Wafer = 'VP51165-15' THEN 'LVP3b'
            WHEN @Wafer = 'VP51165-16' THEN 'LVP3b'
            WHEN @Wafer = 'VP51165-17' THEN 'LVP3b'
            WHEN @Wafer = 'VP51165-18' THEN 'LVP3b'

            WHEN @Wafer = 'VP51165-02' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-03' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-04' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-06' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-07' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-08' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-09' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-10' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-11' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-12' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-13' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-14' THEN 'LVP3c'
            WHEN @Wafer = 'VP51165-23' THEN 'LVP3c'

            WHEN @Wafer = 'VP51344-11' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-12' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-13' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-14' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-15' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-16' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-17' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-18' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-19' THEN 'LVP4a'
            WHEN @Wafer = 'VP51344-20' THEN 'LVP4a'

            WHEN @Wafer = 'VP51344-07' THEN 'LVP4b'
            WHEN @Wafer = 'VP51344-08' THEN 'LVP4b'
            WHEN @Wafer = 'VP51344-09' THEN 'LVP4b'
            WHEN @Wafer = 'VP51344-10' THEN 'LVP4b'

            WHEN @Wafer = 'VP51344-02' THEN 'LVP4c'
            WHEN @Wafer = 'VP51344-03' THEN 'LVP4c'
            WHEN @Wafer = 'VP51344-04' THEN 'LVP4c'
            WHEN @Wafer = 'VP51344-05' THEN 'LVP4c'
            WHEN @Wafer = 'VP51344-06' THEN 'LVP4c'

            WHEN @Wafer = 'VP51604-03' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-07' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-13' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-17' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-05' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-11' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-15' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-21' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-23' THEN 'LVP7/4.15'
            WHEN @Wafer = 'VP51604-24' THEN 'LVP7/4.15'

            WHEN @Wafer = 'VP51429-02' THEN 'LVP7/4.13'
            WHEN @Wafer = 'VP51429-07' THEN 'LVP7/4.13'
            WHEN @Wafer = 'VP51429-17' THEN 'LVP7/4.13'
            WHEN @Wafer = 'VP51429-19' THEN 'LVP7/4.13'
            WHEN @Wafer = 'VP51429-21' THEN 'LVP7/4.13'

            WHEN @Wafer = 'VP51604-04' THEN '4.14'
            WHEN @Wafer = 'VP51604-06' THEN '4.14'
            WHEN @Wafer = 'VP51604-08' THEN '4.14'
            WHEN @Wafer = 'VP51604-09' THEN '4.14'
            WHEN @Wafer = 'VP51604-12' THEN '4.14'
            WHEN @Wafer = 'VP51604-14' THEN '4.14'
            WHEN @Wafer = 'VP51604-16' THEN '4.14'
            WHEN @Wafer = 'VP51604-19' THEN '4.14'
            WHEN @Wafer = 'VP51604-20' THEN '4.14'
            WHEN @Wafer = 'VP51604-22' THEN '4.14'

            WHEN @Wafer = 'PB015315-31' THEN 'xFL2B'
            WHEN @Wafer = 'PB015315-35' THEN 'xFL2B'
            WHEN @Wafer = 'PB015315-37' THEN 'xFL2B'

            WHEN @Wafer = 'PBT25228-23' THEN 'xFL2C'
            WHEN @Wafer = 'PBT25228-22' THEN 'xFL2C'
            WHEN @Wafer = 'PBT25228-21' THEN 'xFL2C'
            WHEN @Wafer = 'PBT25228-20' THEN 'xFL2C'
            WHEN @Wafer = 'PBT25228-19' THEN 'xFL2C'

            WHEN @Wafer = 'PB015315-44' THEN 'xFL2D'
            WHEN @Wafer = 'PB015315-42' THEN 'xFL2D'

            WHEN @Wafer = 'PB015315-30' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-27' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-29' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-26' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-32' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-34' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-36' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-39' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-40' THEN 'xFL2E'
            WHEN @Wafer = 'PB015315-41' THEN 'xFL2E'

            WHEN @Wafer = 'PBT44230-01' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-02' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-03' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-04' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-05' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-06' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-07' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-08' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-09' THEN 'FL2'
            WHEN @Wafer = 'PBT44230-10' THEN 'FL2'

            WHEN @Wafer = 'PBT44230-11' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-12' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-13' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-14' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-15' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-16' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-17' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-18' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-19' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-20' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-21' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-22' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-23' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-24' THEN 'FL3'
            WHEN @Wafer = 'PBT44230-25' THEN 'FL3'

            WHEN @Wafer = 'PBT47034-01' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-02' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-03' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-04' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-05' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-06' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-07' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-08' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-09' THEN 'FL4'
            WHEN @Wafer = 'PBT47034-10' THEN 'FL4'

            WHEN @Wafer = 'VP52388-06' THEN 'LVP14b'
            WHEN @Wafer = 'VP52388-07' THEN 'LVP14b'
            WHEN @Wafer = 'VP52388-08' THEN 'LVP14b'

            WHEN @Wafer = 'VP52460-21' THEN 'LVP15b'
            WHEN @Wafer = 'VP52460-22' THEN 'LVP15b'
            WHEN @Wafer = 'VP52460-23' THEN 'LVP15b'

            WHEN @Wafer = 'PU11600-01' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-02' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-03' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-04' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-05' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-06' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-07' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-08' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-09' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-10' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-11' THEN 'FL7a'
            WHEN @Wafer = 'PU11600-23' THEN 'FL7a'

            WHEN @Wafer = 'PU11600-13' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-14' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-15' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-16' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-17' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-18' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-19' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-20' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-21' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-22' THEN 'FL7b'
            WHEN @Wafer = 'PU11600-24' THEN 'FL7'
            WHEN @Wafer = 'PU11600-25' THEN 'FL7'

            WHEN @Wafer LIKE 'PBU05139-%' THEN 'FL6'
            WHEN @Wafer LIKE 'PBU13600-%' THEN 'FL8'
            WHEN @Wafer LIKE 'PBU15602-%' THEN 'FL9'
            WHEN @Wafer LIKE 'PU19292-%' THEN 'RP1'
            WHEN @Wafer LIKE 'PU20057-%' THEN 'RP2'
            WHEN @Wafer LIKE 'PU21270-%' THEN 'RP3'
            WHEN @Wafer LIKE 'PU22381-%' THEN 'RP4'
            WHEN @Wafer LIKE 'PU23127-%' THEN 'RP5'
            WHEN @Wafer LIKE 'PU24086-%' THEN 'RP6'
            WHEN @Wafer LIKE 'PU24087-%' THEN 'RP7'
            WHEN @Wafer LIKE 'PU25225-%' THEN 'RP8'
            WHEN @Wafer LIKE 'PU27316-%' THEN 'RP9'
            WHEN @Wafer LIKE 'PU27317-%' THEN 'RP10'
            WHEN @Wafer LIKE 'PU27318-%' THEN 'RP11'
            WHEN @Wafer LIKE 'PU28331-%' THEN 'RP12'
            WHEN @Wafer LIKE 'PU28332-%' THEN 'RP13'
            WHEN @Wafer LIKE 'PU28333-%' THEN 'RP14'
            WHEN @Wafer LIKE 'PU29042-%' THEN 'RP15'
            WHEN @Wafer LIKE 'PU29043-%' THEN 'RP16'
            WHEN @Wafer LIKE 'PU29325-%' THEN 'RP17'
            WHEN @Wafer LIKE 'PU30183-%' THEN 'RP18'
            WHEN @Wafer LIKE 'PU30184-%' THEN 'RP19'
            WHEN @Wafer LIKE 'PU32048-%' THEN 'RP20'
            WHEN @Wafer LIKE 'PU32049-%' THEN 'RP21'
            WHEN @Wafer LIKE 'PU32050-%' THEN 'RP22'
            WHEN @Wafer LIKE 'PU33246-%' THEN 'RP23'
            WHEN @Wafer LIKE 'PU34009-%' THEN 'RP24'
            WHEN @Wafer LIKE 'PU34188-%' THEN 'RP25'
            WHEN @Wafer LIKE 'PU35193-%' THEN 'RP26'
            WHEN @Wafer LIKE 'PU35213-%' THEN 'RP27'
            WHEN @Wafer LIKE 'PU36196-%' THEN 'RP28'
            WHEN @Wafer LIKE 'PU36319-%' THEN 'RP29'
            WHEN @Wafer LIKE 'PU36344-%' THEN 'RP30'
            WHEN @Wafer LIKE 'PU37000-%' THEN 'RP31'
            WHEN @Wafer LIKE 'PU37001-%' THEN 'RP32'
            WHEN @Wafer LIKE 'PU40202-%' THEN 'RP33'
            WHEN @Wafer LIKE 'PU40314-%' THEN 'RP34'
            WHEN @Wafer LIKE 'PU40330-%' THEN 'RP35'
            WHEN @Wafer LIKE 'PU41204-%' THEN 'RP36'
            WHEN @Wafer LIKE 'PU41323-%' THEN 'RP37'
            WHEN @Wafer LIKE 'PU42035-%' THEN 'RP38'
            WHEN @Wafer LIKE 'PU42315-%' THEN 'RP39'

            WHEN @Wafer LIKE 'PU43243-%' THEN 'RP40'
            WHEN @Wafer LIKE 'PU43244-%' THEN 'RP41'
            WHEN @Wafer LIKE 'PU44279-%' THEN 'RP42'
            WHEN @Wafer LIKE 'PU44280-%' THEN 'RP43'
            WHEN @Wafer LIKE 'PU45025-%' THEN 'RP44'
            WHEN @Wafer LIKE 'PU45610-%' THEN 'RP45'
            WHEN @Wafer LIKE 'PU46001-%' THEN 'RP46'
            WHEN @Wafer LIKE 'PU46250-%' THEN 'RP47'
            WHEN @Wafer LIKE 'PU47160-%' THEN 'RP48'
            WHEN @Wafer LIKE 'PU47281-%' THEN 'RP49'
            WHEN @Wafer LIKE 'PU48123-%' THEN 'RP50'
            WHEN @Wafer LIKE 'PU48257-%' THEN 'RP51'
            WHEN @Wafer LIKE 'PU49099-%' THEN 'RP52'
            WHEN @Wafer LIKE 'PU49258-%' THEN 'RP53'
            WHEN @Wafer LIKE 'PU50105-%' THEN 'RP54'
            WHEN @Wafer LIKE 'PU50106-%' THEN 'RP55'
            WHEN @Wafer LIKE 'PU51109-%' THEN 'RP56'
            WHEN @Wafer LIKE 'PU51110-%' THEN 'RP57'
            WHEN @Wafer LIKE 'PU52122-%' THEN 'RP58'
            WHEN @Wafer LIKE 'PU52123-%' THEN 'RP59'
            WHEN @Wafer LIKE 'PU53000-%' THEN 'RP60'
            WHEN @Wafer LIKE 'PU53256-%' THEN 'RP61'
            WHEN @Wafer LIKE 'PV02210-%' THEN 'RP62'
            WHEN @Wafer LIKE 'PV02315-%' THEN 'RP63'
            WHEN @Wafer LIKE 'PV03000-%' THEN 'RP64'
            WHEN @Wafer LIKE 'PV03292-%' THEN 'RP65'
            WHEN @Wafer LIKE 'PV04028-%' THEN 'RP66'
            WHEN @Wafer LIKE 'PV05273-%' THEN 'RP67'
            WHEN @Wafer LIKE 'PV05304-%' THEN 'RP68'
            WHEN @Wafer LIKE 'PV06065-%' THEN 'RP69'
            WHEN @Wafer LIKE 'PV07202-%' THEN 'RP70'
            WHEN @Wafer LIKE 'PV07299-%' THEN 'RP71'
            WHEN @Wafer LIKE 'PV08000-%' THEN 'RP72'
            WHEN @Wafer LIKE 'PV09089-%' THEN 'RP73'
            WHEN @Wafer LIKE 'PV15106-%' THEN 'RP74'
            WHEN @Wafer LIKE 'PV16105-%' THEN 'RP75'
            WHEN @Wafer LIKE 'PV18252-%' THEN 'RP76'
            WHEN @Wafer LIKE 'PV19004-%' THEN 'RP77'
            WHEN @Wafer LIKE 'PV19005-%' THEN 'RP78'
            WHEN @Wafer LIKE 'PV20096-%' THEN 'RP79'
            WHEN @Wafer LIKE 'PV21129-%' THEN 'RP80'
            WHEN @Wafer LIKE 'PV21130-%' THEN 'RP81'
            WHEN @Wafer LIKE 'PV22004-%' THEN 'RP82'
            WHEN @Wafer LIKE 'PV23038-%' THEN 'RP83'
            WHEN @Wafer LIKE 'PV23039-%' THEN 'RP84'
            WHEN @Wafer LIKE 'PV24032-%' THEN 'RP85'
            WHEN @Wafer LIKE 'PV25006-%' THEN 'RP86'
            WHEN @Wafer LIKE 'PV25007-%' THEN 'RP87'
            WHEN @Wafer LIKE 'PV26041-%' THEN 'RP88'
            WHEN @Wafer LIKE 'PV26042-%' THEN 'RP89'
            WHEN @Wafer LIKE 'PV27098-%' THEN 'RP90'
            WHEN @Wafer LIKE 'PV27099-%' THEN 'RP91'
            WHEN @Wafer LIKE 'PV28085-%' THEN 'RP92'
            WHEN @Wafer LIKE 'PV28086-%' THEN 'RP93'
            WHEN @Wafer LIKE 'PV29013-%' THEN 'RP94'
            WHEN @Wafer LIKE 'PV29014-%' THEN 'RP95'
            WHEN @Wafer LIKE 'PV30111-%' THEN 'RP96'
            WHEN @Wafer LIKE 'PV30113-%' THEN 'RP97'
            WHEN @Wafer LIKE 'PV31159-%' THEN 'RP98'
            WHEN @Wafer LIKE 'PV31183-%' THEN 'RP99'
            WHEN @Wafer LIKE 'PV32043-%' THEN 'RP100'
            WHEN @Wafer LIKE 'PV32044-%' THEN 'RP101'
            WHEN @Wafer LIKE 'PV33069-%' THEN 'RP102'
            WHEN @Wafer LIKE 'PV33137-%' THEN 'RP103'
            WHEN @Wafer LIKE 'PV34045-%' THEN 'RP104'
            WHEN @Wafer LIKE 'PV34046-%' THEN 'RP105'
            WHEN @Wafer LIKE 'PV34186-%' THEN 'RP106'
            WHEN @Wafer LIKE 'PV35036-%' THEN 'RP107'
            WHEN @Wafer LIKE 'PV35038-%' THEN 'RP108'
            WHEN @Wafer LIKE 'PV35039-%' THEN 'RP109'
            WHEN @Wafer LIKE 'PV36000-%' THEN 'RP110'
            WHEN @Wafer LIKE 'PV36001-%' THEN 'RP111'
            WHEN @Wafer LIKE 'PV39085-%' THEN 'RP112'
            WHEN @Wafer LIKE 'PV40088-%' THEN 'RP113'
            WHEN @Wafer LIKE 'PV40089-%' THEN 'RP114'
            WHEN @Wafer LIKE 'PV40090-%' THEN 'RP115'
            WHEN @Wafer LIKE 'PV40091-%' THEN 'RP116'
            WHEN @Wafer LIKE 'PV41019-%' THEN 'RP117'
            WHEN @Wafer LIKE 'PV41020-%' THEN 'RP118'
            WHEN @Wafer LIKE 'PV41071-%' THEN 'RP119'
            WHEN @Wafer LIKE 'PV42024-%' THEN 'RP120'
            WHEN @Wafer LIKE 'PV42025-%' THEN 'RP121'
            WHEN @Wafer LIKE 'PV42026-%' THEN 'RP122'
            WHEN @Wafer LIKE 'PV43000-%' THEN 'RP123'
            WHEN @Wafer LIKE 'PV43001-%' THEN 'RP124'
            WHEN @Wafer LIKE 'PV43002-%' THEN 'RP125'
            WHEN @Wafer LIKE 'PV43158-%' THEN 'RP126'
            WHEN @Wafer LIKE 'PV44117-%' THEN 'RP127'
            WHEN @Wafer LIKE 'PV44118-%' THEN 'RP128'
            WHEN @Wafer LIKE 'PV44119-%' THEN 'RP129'
            WHEN @Wafer LIKE 'PV44120-%' THEN 'RP130'
            WHEN @Wafer LIKE 'PV45056-%' THEN 'RP131'
            WHEN @Wafer LIKE 'PV45057-%' THEN 'RP132'
            WHEN @Wafer LIKE 'PV45127-%' THEN 'RP133'
            WHEN @Wafer LIKE 'PV45128-%' THEN 'RP134'
            WHEN @Wafer LIKE 'PV46001-%' THEN 'RP135'
            WHEN @Wafer LIKE 'PV46002-%' THEN 'RP136'
            WHEN @Wafer LIKE 'PV47030-%' THEN 'RP137'
            WHEN @Wafer LIKE 'PV47031-%' THEN 'RP138'
            WHEN @Wafer LIKE 'PV48030-%' THEN 'RP139'
            WHEN @Wafer LIKE 'PV48031-%' THEN 'RP140'
            WHEN @Wafer LIKE 'PV49003-%' THEN 'RP141'
            WHEN @Wafer LIKE 'PV49004-%' THEN 'RP142'
            WHEN @Wafer LIKE 'PV50026-%' THEN 'RP143'
            WHEN @Wafer LIKE 'PV50027-%' THEN 'RP144'
            WHEN @Wafer LIKE 'PV51024-%' THEN 'RP145'
            WHEN @Wafer LIKE 'PV51025-%' THEN 'RP146'
            WHEN @Wafer LIKE 'PV52004-%' THEN 'RP147'
            WHEN @Wafer LIKE 'PV52005-%' THEN 'RP148'
            WHEN @Wafer LIKE 'PW01000-%' THEN 'RP149'
            WHEN @Wafer LIKE 'PW01001-%' THEN 'RP150'
            WHEN @Wafer LIKE 'PW02108-%' THEN 'RP151'
            WHEN @Wafer LIKE 'PW02109-%' THEN 'RP152'
            WHEN @Wafer LIKE 'PW03018-%' THEN 'RP153'
            WHEN @Wafer LIKE 'PW03019-%' THEN 'RP154'
            WHEN @Wafer LIKE 'PW04040-%' THEN 'RP155'
            WHEN @Wafer LIKE 'PW04041-%' THEN 'RP156'
            WHEN @Wafer LIKE 'PW05074-%' THEN 'RP157'
            WHEN @Wafer LIKE 'PW05075-%' THEN 'RP158'
            WHEN @Wafer LIKE 'PW06145-%' THEN 'RP159'
            WHEN @Wafer LIKE 'PW07031-%' THEN 'RP160'
            WHEN @Wafer LIKE 'PW08074-%' THEN 'RP161'
            WHEN @Wafer LIKE 'PW09062-%' THEN 'RP162'
            WHEN @Wafer LIKE 'PW10022-%' THEN 'RP163'
            WHEN @Wafer LIKE 'PW11000-%' THEN 'RP164'
            WHEN @Wafer LIKE 'PW12013-%' THEN 'RP165'
            WHEN @Wafer LIKE 'PW13000-%' THEN 'RP166'
            WHEN @Wafer LIKE 'PW14001-%' THEN 'RP167'
            WHEN @Wafer LIKE 'PW15000-%' THEN 'RP168'
            WHEN @Wafer LIKE 'PW16000-%' THEN 'RP169'
            WHEN @Wafer LIKE 'PW17002-%' THEN 'RP170'
            WHEN @Wafer LIKE 'PW18034-%' THEN 'RP171'
            WHEN @Wafer LIKE 'PW19048-%' THEN 'RP172'
            WHEN @Wafer LIKE 'PW24058-%' THEN 'RP173'
            WHEN @Wafer LIKE 'PW26066-%' THEN 'RP174'
            WHEN @Wafer LIKE 'PW28028-%' THEN 'RP175'
            WHEN @Wafer LIKE 'PW30037-%' THEN 'RP176'
            WHEN @Wafer LIKE 'PW32035-%' THEN 'RP177'
            WHEN @Wafer LIKE 'PW36035-%' THEN 'RP178'
            WHEN @Wafer LIKE 'PW40067-%' THEN 'RP179'

            WHEN @Wafer LIKE 'PBU13600-07%' THEN 'FL8c'
            WHEN @Wafer LIKE 'PBU13600-09%' THEN 'FL8c'
            WHEN @Wafer LIKE 'PBU13600-11%' THEN 'FL8c'
            WHEN @Wafer LIKE 'PBU13600-13%' THEN 'FL8c'
            WHEN @Wafer LIKE 'PBU13600-18%' THEN 'FL8c'

            WHEN @Wafer LIKE 'P162155-%' THEN '1C4A'
        END);
      END;
	  
GO

SELECT ncr.EventName EventName, ncr.Initiator Initiator, ncr.Status Status, ncr.Owner Owner,
 ncr.ContainerName ContainerName, cl.ContainerLevelName ContainerLevel, cd.PBI_CustomerWaferName CustomerLot,
 ncr.EventArea EventArea,ncr.Description Description  ,ncr.OccurrenceDate OccurrenceDate  ,
 ncr.OperationName OperationName, ncr.Product Product, ncr.Qty Qty,
 ncr.CloseDate CloseDate, ncr.ClosedBy ClosedBy, ncr.Resolution Resolution, ncr.ResolutionComments ResolutionComments
 INTO #ncrQueryList
FROM [CamODS_6X].[insitedb_schema].[PBI_GetEventNameforContainer] ncr
  JOIN [CamODS_6X].[insitedb_schema].Container c
  ON ncr.ContainerName = c.ContainerName
  JOIN  [CamODS_6X].[insitedb_schema].ContainerDetail cd
  ON c.DetailId = cd.ContainerDetailId
  JOIN [CamODS_6X].[insitedb_schema].ContainerLevel cl
  ON c.LevelId = cl.ContainerLevelId
  where ( cl.ContainerLevelName = 'WAFER' or cl.ContainerLevelName = 'TRAY' or cl.ContainerLevelName = 'CHIP' ) ;


--  stringCustomerLots as(
--	select distinct CustomerLot 
--	from ncrQueryList
	--order by CustomerLot
--  )



 With waferQueryList as( 
	select c.ContainerName WaferContainerName, cd.PBI_CustomerWaferName CustomerLot
	from [CamODS_6X].[insitedb_schema].Container c
	join [CamODS_6X].[insitedb_schema].ContainerLevel cl
	on c.LevelId = cl.ContainerLevelId
	join [CamODS_6X].[insitedb_schema].ContainerDetail cd 
	on c.ContainerId = cd.ContainerId
	where (cl.ContainerLevelName = 'WAFER') and 
	(cd.PBI_CustomerWaferName in (select distinct CustomerLot from #ncrQueryList)) and 
	(c.ContainerName  like '[a-zA-Z][a-zA-Z][0-9][0-9][0-9][0-9][0-9][-][0-9][0-9]')
	or (c.ContainerName  like '[a-zA-Z][a-zA-Z0-9][a-zA-Z][0-9][0-9][0-9][0-9][0-9][-][0-9][0-9]')
 )

 select EventName = ncrq.EventName,
                     Initiator = ncrq.Initiator,
                     Status = ncrq.Status,
                     Owner = ncrq.Owner,
                     ContainerName = ncrq.ContainerName,
                     WaferContainerName = wq.WaferContainerName,
                    -- BatchAlias = bq.BatchAlias,
					 --BatchAlias = dbo.BatchAlias(wq.WaferContainerName),
                     ContainerLevel = ncrq.ContainerLevel,
                     CustomerLot = ncrq.CustomerLot,
                     EventArea = ncrq.EventArea,
                     Description = ncrq.Description,
                     OccurrenceDate = ncrq.OccurrenceDate,
                     OperationName = ncrq.OperationName,
                     Product = ncrq.Product,
                     Qty = ncrq.Qty,
                     CloseDate = ncrq.CloseDate,
                     ClosedBy = ncrq.ClosedBy,
                     Resolution = ncrq.Resolution,
                     ResolutionComments = ncrq.ResolutionComments
 INTO #ncrQuery
 from #ncrQueryList ncrq
 join waferQueryList wq
 on ncrq.CustomerLot = wq.CustomerLot

 GO

 -- Region BatchAliasQuery

WITH
	[DistinctBatches] ([BatchName], [AliasBatchName], [RowNumber]) AS
	(
		SELECT
			[Batch].[BatchName] AS [BatchName],
			TRIM(REPLACE(REPLACE(REPLACE([Batch].[AliasBatchName],
				'Lot', ''),
				'IMEC', ''),
				'Silterra', '')) AS [AliasBatchName],
			ROW_NUMBER() OVER (PARTITION BY [Batch].[BatchName] ORDER BY [Batch].[AliasBatchName]) AS [RowNumber]
		FROM [SequelMetrology].[dbo].[Batch] AS [Batch]
		WHERE [Batch].[AliasBatchName] IS NOT NULL
	)
SELECT
	distinct [ncrQuery].[WaferContainerName] AS [ContainerName],
	COALESCE(dbo.BatchAlias([ncrQuery].[WaferContainerName]), [Batches].[AliasBatchName]) AS [AliasBatchName]
INTO #BatchAliasQuerySequel
FROM #ncrQuery AS [ncrQuery]
LEFT JOIN [DistinctBatches] AS [Batches] ON [ncrQuery].[WaferContainerName] LIKE CONCAT([Batches].[BatchName], '-%')
WHERE [Batches].[RowNumber] = 1;


WITH [DistinctBatches] ([BatchName], [AliasBatchName]) AS
(SELECT
	[SpiderBatch].[BatchName],
	SUBSTRING(
		[SpiderBatch].[AliasBatchName],
		LEN([SpiderBatch].[AliasBatchName]) - CHARINDEX(';', REVERSE([SpiderBatch].[AliasBatchName])) + 2,
		LEN([SpiderBatch].[AliasBatchName])) AS [SpiderAlias]
FROM [SpiderMetrology].[dbo].[Batch] AS [SpiderBatch])
SELECT
	distinct [ncrQuery].[WaferContainerName] AS [ContainerName],
	[Batches].[AliasBatchName] AS [AliasBatchName]
	--COALESCE(dbo.BatchAlias([ncrQuery].[WaferContainerName]), [Batches].[AliasBatchName]) AS [AliasBatchName]
INTO #BatchAliasQuerySpider
FROM #ncrQuery AS [ncrQuery]
LEFT JOIN [DistinctBatches] AS [Batches] ON [ncrQuery].[WaferContainerName] LIKE CONCAT([Batches].[BatchName], '-%')
where [AliasBatchName] is not null


GO




-- EndRegion
WITH BatchAlias AS
(SELECT ContainerName, AliasBatchName FROM  #BatchAliasQuerySequel
UNION ALL
SELECT ContainerName, AliasBatchName FROM  #BatchAliasQuerySpider
)
SELECT ncr.EventName, ncr.Initiator, ncr.Status, ncr.Owner, ncr.ContainerName, ncr.WaferContainerName, BatchAlias.AliasBatchName as BatchAlias, ncr.CustomerLot, ncr.EventArea, 
ncr.Description, ncr.OccurrenceDate, ncr.OperationName, ncr.Product, ncr.Qty, ncr.CloseDate, ncr.ClosedBy, ncr.Resolution, ncr.ResolutionComments, ncr.ContainerLevel
FROM #ncrQuery ncr
left join BatchAlias
ON ncr.[WaferContainerName] = BatchAlias.ContainerName
--WHERE ncr.ContainerName in ('PV16105-12','AX08108-16','AX16003-21','AX52215-01','AY06153-25','AY13099-22','PV19004-15','AX08107-04','AX08109-02','AX08110-25','AX17033-01','AX45216-06','AY06153-17','AY15113-08','AW47106-22','AY06153-03','AY22264-10','PV03000-15','PU19292-24_Defects','AX08108-03','AX51177-14','AY15113-12','AY20293-15','AY22265-14','PV03000-22','PV41019-14_Rework','AX08107-25','AY22265-08','AY17047-18','PV42025-06','AY06153-21','AY23265-20','PV04028-13','PV18252-14','AY02185-12','AX14095-02','AX15006-02','AY06153-09','AY27185-12','PV15106-09','AX15006-05','AY02185-03','0490AB82894981','AY22264-22','AY26206-24','AY02185-14','AY27185-13','AX08107-18','048E9BE2045780','AY16231-11','AY18266-14','AY22264-20','AY22265-23','AX14095-03','AX08108-15','AX08108-16','AX49206-10','AY06153-21','PV03000-11','AW47070-05','AX14095-20','AY15211-01','AY27186-03','PV20096-13','AW47106-13','AX08109-16','AX34164-03','AY06153-03','AY15113-13','PV20096-11','AX08108-19','AX08109-03','AX20130-09','0494E182894980','0494F382894980','AY06153-21','AY09278-06','AX01021-09','AX08108-23','AX36000-24','AY06153-03','AY18266-13','AY27187-20','PV02210-25','AW47070-24','AX01021-10','AX01021-13','AY15113-14','PV18252-13','AX15006-04','AX08110-12','AX17033-20','AX17033-04','AW47106-09','AW47070-17','AX17033-16','PV27099-15','0493BA82894980','AX01021-01','AX17033-07')

